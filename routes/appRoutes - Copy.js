var express = require('express');
var router = express();
var request = require('request');
var fs = require('fs');
var jwt = require('jsonwebtoken');
var labels = [];
var labels_tmp;
var arr_labels = ["manufacturer", "tax_class_id", "gift_message_available", "brand", "grsns_product_type", "grsns_voltage", "frequency", "power_rating", 
"dimension", "nki_voltage", "nki_special_features", "nki_manufaturename", "grinding_speed", "tool_dimension"];
var magentoServerKey;
var jsonSecretkey;

var ERROR = {
    JSON_ERROR              : "USER SESSION IS INVALID",                            // 400
    HTTP_UNAUTHORIZED       : "INVALID CREDENTIALS",                                // 401
    HTTP_FORBIDDEN          : "ACCESS DENIED",                                      // 403
    HTTP_NOT_FOUND          : "RESOURCE NOT FOUND",                                 // 404
    HTTP_METHOD_NOT_ALLOWED : "YOU'RE NOT ALLOWED TO ACCESS THIS RESOURCE",         // 405
    HTTP_NOT_ACCEPTABLE     : "THE REQUEST IS NOT ACCEPTABLE",                      // 406
    HTTP_INTERNAL_ERROR     : "RESOURCE INTERNAL ERROR",                            // 500
    CUSTOM_ERROR            : "IT'S NOT YOU, IT'S US. SORRY FOR THE INCONVENIENCE"  // CUSTOM ERROR
}


router.get('/get_category', (req, res) => {
    request(magentoServerKey + '/rest/V1/categories', function (error, response, body) {
        if (!error && response.statusCode == 200) {
            res.send(JSON.parse(body))
        }
    })
});

router.get('/get_labels', (req, res) => {
    res.send(labels);
});

router.get('/get_sku/:id', (req, res) => {
    request(magentoServerKey + '/rest/V1/products/'+req.params.id, function (error, response, body) {
        if (response) {
            res.send(JSON.parse(body))
        }
        if(error) {
            if(error.statusCode == 401) {
                res.status(400).send(ERROR.HTTP_UNAUTHORIZED)
            }
            if(error.statusCode == 403) {
                res.status(400).send(ERROR.HTTP_FORBIDDEN)
            }
            if(error.statusCode == 404) {
                res.status(400).send(ERROR.HTTP_NOT_FOUND)
            }
            if(error.statusCode == 405) {
                res.status(400).send(ERROR.HTTP_METHOD_NOT_ALLOWED)
            }
            if(error.statusCode == 406) {
                res.status(400).send(ERROR.HTTP_NOT_ACCEPTABLE)
            }
            if(error.statusCode == 500) {
                res.status(400).send(ERROR.HTTP_INTERNAL_ERROR)
            }
        }
    })
});

router.get('/get_products/:id', (req, res) => {
    request(magentoServerKey + '/rest/V1/products?searchCriteria[filter_groups][0][filters][0][field]=category_id&searchCriteria[filter_groups][0][filters][0][value]='+req.params.id, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            res.send(JSON.parse(body))
        }
    })
});

router.get('/user_login/:id', (req, res) => {

    var loginData = req.params.id;

    request.post({
        url: magentoServerKey + '/rest/default/V1/integration/customer/token',
        headers: { "Content-Type": "application/json", "cache-control": "no-cache" },
        body: loginData,
      }, function(error, response, body){
        if (response) {
            try {
                var token = createToken(body.toString().replace(/"/g, ''))    
                res.send(token)
            }
            catch(e) {
                res.status(401).send(ERROR.JSON_ERROR);
            }
        }
        if(error) {
            res.status(error.statusCode).send({
                message: error.message
            })
        }
    });
});









// Cart Functions


router.get('/get_cart_items/:id', (req, res) => {

    var Jwt = req.params.id;
    try {
        var token = decodeToken(Jwt);
        
        request.get({
            headers: { "Content-Type": "application/json", "Authorization": "Bearer " + token, "cache-control": "no-cache" },
            url: magentoServerKey + '/rest/default/V1/carts/mine'
          }, function(error, response, body){
            if (response) {
                try {
                    var token = createToken(body.toString())
                    res.send(token)
                }
                catch(e) {
                    res.status(400).send(ERROR.JSON_ERROR);
                }
            }
            if(error) {
                res.status(error.statusCode).send({
                    message: error.message
                })
            }
        });
    }
    catch(e) {
        res.status(400).send(ERROR.JSON_ERROR);
    }
});


router.get('/get_cartId/:id', (req, res) => {

    var Jwt = req.params.id;
    try {
        var token = decodeToken(Jwt);
        
        request.post({
            url: magentoServerKey + 'rest/default/V1/carts/mine',
            headers: { "Content-Type": "application/json", "Authorization": "Bearer " + token, "cache-control": "no-cache" }
          }, function(error, response, body){
            if (response) {
                try {
                    var token = createToken(body.toString())    
                    res.send(token)
                }
                catch(e) {
                    res.status(400).send(ERROR.JSON_ERROR);
                }
            }
            if(error) {
                res.status(error.statusCode).send({
                    message: error.message
                })
            }
        });
    }
    catch(e) {
        res.status(400).send(ERROR.JSON_ERROR);
    }
})

router.get('/add_cart_items/:input', (req, response) => {

    var http = require("http");
    var Jwt = JSON.parse(req.params.input);
    var token = decodeToken(Jwt.jwt);

    var options = {
    "method": "POST",
    "hostname": "115.111.129.98",
    "path": "/elec/rest/default/V1/carts/mine/items",
    "headers": {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + token.toString(),
        "cache-control": "no-cache",
    }
    };

    var req = http.request(options, function (res) {
        var chunks = [];
      
        res.on("data", function (chunk) {
          chunks.push(chunk);
        });
      
        res.on("end", function () {
          var body = Buffer.concat(chunks);
          var token = createToken(body.toString())
          response.send(token)
        });
      });
      
      req.write(JSON.stringify(Jwt.items));
      req.end();



    try {
        var token = decodeToken(Jwt);
        
        request.post({
            url: magentoServerKey + 'rest/default/V1/carts/mine',
            headers: { "Content-Type": "application/json", "Authorization": "Bearer " + token, "cache-control": "no-cache" },
            body: JSON.stringify(Jwt.items)
          }, function(error, response, body){
            if (response) {
                try {
                    var token = createToken(body.toString())    
                    res.send(token)
                }
                catch(e) {
                    res.status(400).send(ERROR.JSON_ERROR);
                }
            }
            if(error) {
                res.status(error.statusCode).send({
                    message: error.message
                })
            }
        });
    }
    catch(e) {
        res.status(400).send(ERROR.JSON_ERROR);
    }
})


router.get('/update_cart_item/:input', (req, response) => {

    var http = require("http");
    var Jwt = JSON.parse(req.params.input);
    var token = decodeToken(Jwt.jwt);

    var options = {
    "method": "PUT",
    "hostname": "115.111.129.98",
    "path": "/elec/rest/default/V1/carts/mine/items" + Jwt.id.toString(),
    "headers": {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + token.toString(),
        "cache-control": "no-cache",
    }
    };

    var req = http.request(options, function (res) {
        var chunks = [];
      
        res.on("data", function (chunk) {
          chunks.push(chunk);
        });
      
        res.on("end", function () {
          var body = Buffer.concat(chunks);
          var token = createToken(body.toString())
          response.send(token)
        });
      });
      
      req.write(JSON.stringify(Jwt.items));
      req.end();
})

router.get('/delete_cart_item/:input', (req, response) => {

    var http = require("http");
    var Jwt = JSON.parse(req.params.input);
    var token = decodeToken(Jwt.jwt);

    var options = {
    "method": "DELETE",
    "hostname": "115.111.129.98",
    "path": "/elec/rest/default/V1/carts/mine/items" + Jwt.id.toString(),
    "headers": {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + token.toString(),
        "cache-control": "no-cache",
    }
    };

    var req = http.request(options, function (res) {
        var chunks = [];
      
        res.on("data", function (chunk) {
          chunks.push(chunk);
        });
      
        res.on("end", function () {
          var body = Buffer.concat(chunks);
          var token = createToken(body.toString())
          response.send(token)
        });
      });
      
    req.end();
})


// Guest Cart Functions

router.get('/create_guest_cart', (req, response) => {

    var http = require("http");

    var options = {
    "method": "POST",
    "hostname": "115.111.129.98",
    "path": "/elec/rest/V1/guest-carts",
    "headers": {
        "Content-Type": "application/json",
        "cache-control": "no-cache",
    }
    };

    var req = http.request(options, function (res) {
    var chunks = [];

    res.on("data", function (chunk) {
        chunks.push(chunk);
    });

    res.on("end", function () {
        var body = Buffer.concat(chunks);
        var token = createToken(body.toString())
        response.send(token)
    });
    });

    req.end();
})


router.get('/add_guest_cart_items/:input', (req, response) => {

    var http = require("http");
    var Jwt = JSON.parse(req.params.input);
    var token = decodeToken(Jwt.jwt);

    var options = {
    "method": "POST",
    "hostname": "115.111.129.98",
    "path": "/elec/rest/V1/guest-carts/" + token.toString() + "/items",
    "headers": {
        "Content-Type": "application/json",
        "cache-control": "no-cache",
    }
    };

    var req = http.request(options, function (res) {
        var chunks = [];
      
        res.on("data", function (chunk) {
          chunks.push(chunk);
        });
      
        res.on("end", function () {
          var body = Buffer.concat(chunks);
          var token = createToken(body.toString())
          response.send(token)
        });
      });
      
      req.write(JSON.stringify(Jwt.items));
      req.end();
})

router.get('/get_guest_cart_items/:id', (req, response) => {

    var http = require("http");
    var Jwt = req.params.id;
    var token = decodeToken(Jwt).replace(/"/g, '');

    var options = {
    "method": "GET",
    "hostname": "115.111.129.98",
    "path": "/elec/rest/V1/guest-carts/" + token.toString() + "/items",
    "headers": {
        "Content-Type": "application/json",
        "cache-control": "no-cache",
    }
    };

    var req = http.request(options, function (res) {
    var chunks = [];

    res.on("data", function (chunk) {
        chunks.push(chunk);
    });

    res.on("end", function () {
        var body = Buffer.concat(chunks);
        var token = createToken(body.toString())
        response.send(token)
    });
    });

    req.end();
});


router.get('/update_guest_cart_item/:input', (req, response) => {

    var http = require("http");
    var Jwt = JSON.parse(req.params.input);
    var token = decodeToken(Jwt.jwt).replace(/"/g, '');

    var options = {
    "method": "PUT",
    "hostname": "115.111.129.98",
    "path": "/elec/rest/V1/guest-carts/" + token.toString() + "/items/" + Jwt.id.toString(),
    "headers": {
        "Content-Type": "application/json",
        "cache-control": "no-cache",
    }
    };

    var req = http.request(options, function (res) {
        var chunks = [];
      
        res.on("data", function (chunk) {
          chunks.push(chunk);
        });
      
        res.on("end", function () {
          var body = Buffer.concat(chunks);
          var token = createToken(body.toString())
          response.send(token)
        });
      });
      
      req.write(JSON.stringify(Jwt.items));
      req.end();
})

router.get('/delete_guest_cart_item/:input', (req, response) => {

    var http = require("http");
    var Jwt = JSON.parse(req.params.input);
    var token = decodeToken(Jwt.jwt).replace(/"/g, '');

    var options = {
    "method": "DELETE",
    "hostname": "115.111.129.98",
    "path": "/elec/rest/V1/guest-carts/" + token.toString() + "/items/" + Jwt.id.toString(),
    "headers": {
        "Content-Type": "application/json",
        "cache-control": "no-cache",
    }
    };

    var req = http.request(options, function (res) {
        var chunks = [];
      
        res.on("data", function (chunk) {
          chunks.push(chunk);
        });
      
        res.on("end", function () {
          var body = Buffer.concat(chunks);
          var token = createToken(body.toString())
          response.send(token)
        });
      });
      
    req.end();
})





// Address Functions

router.get('/get_shipping_address/:id', (req, response) => {

    var http = require("http");
    var Jwt = req.params.id;
    var token = decodeToken(Jwt);

    var options = {
    "method": "GET",
    "hostname": "115.111.129.98",
    "path": "/elec/rest/V1/customers/me/shippingAddress",
    "headers": {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + token,
        "cache-control": "no-cache",
    }
    };

    var req = http.request(options, function (res) {
    var chunks = [];
    
    res.on("data", function (chunk) {
        chunks.push(chunk);
    });

    res.on("end", function () {
        var body = Buffer.concat(chunks);
        var token = createToken(body.toString())
        response.send(token)
    });
    });

    req.end();
});

router.get('/get_billing_address/:id', (req, response) => {

    var http = require("http");
    var Jwt = req.params.id;
    var token = decodeToken(Jwt);

    var options = {
    "method": "GET",
    "hostname": "115.111.129.98",
    "path": "/elec/rest/V1/customers/me/billingAddress",
    "headers": {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + token,
        "cache-control": "no-cache",
    }
    };

    var req = http.request(options, function (res) {
    var chunks = [];

    res.on("data", function (chunk) {
        chunks.push(chunk);
    });

    res.on("end", function () {
        var body = Buffer.concat(chunks);
        var token = createToken(body.toString())
        response.send(token)
    });
    });

    req.end();
});




// Shipping Functions


router.get('/estimate_shipping_methods/:input', (req, response) => {

    var http = require("http");
    var Jwt = JSON.parse(req.params.input);
    var token = decodeToken(Jwt.jwt);

    var options = {
    "method": "POST",
    "hostname": "115.111.129.98",
    "path": "/elec/rest/default/V1/carts/mine/estimate-shipping-methods",
    "headers": {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + token.toString(),
        "cache-control": "no-cache",
    }
    };

    var req = http.request(options, function (res) {
        var chunks = [];
      
        res.on("data", function (chunk) {
          chunks.push(chunk);
        });
      
        res.on("end", function () {
          var body = Buffer.concat(chunks);
          var token = createToken(body.toString())
          response.send(token)
        });
      });
      
    req.write(JSON.stringify(Jwt.items));
    req.end();
})

router.get('/set_shipping_billing_info/:input', (req, response) => {

    var http = require("http");
    var Jwt = JSON.parse(req.params.input);
    var token = decodeToken(Jwt.jwt);

    var options = {
    "method": "POST",
    "hostname": "115.111.129.98",
    "path": "/elec/rest/default/V1/carts/mine/shipping-information",
    "headers": {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + token.toString(),
        "cache-control": "no-cache",
    }
    };

    var req = http.request(options, function (res) {
        var chunks = [];
      
        res.on("data", function (chunk) {
          chunks.push(chunk);
        });
      
        res.on("end", function () {
          var body = Buffer.concat(chunks);
          var token = createToken(body.toString())
          response.send(token)
        });
      });
      
    req.write(JSON.stringify(Jwt.items));
    req.end();
})










function getLabels() {
    var http = require("http");
    var count = 0;

    for(var i = 0; i < arr_labels.length; i++) {
        var options = {
        "method": "GET",
        "hostname": "115.111.129.98",
        "path": "/elec/rest/V1/products/attributes/" + arr_labels[i],
        "headers": {
            "authorization": "Bearer 7667gmes7myxeo3xagtosyrm57mjgck6",
            "content-type": "application/json",
            "cache-control": "no-cache",
        }
        };

        var req = http.request(options, function (res) {
        var chunks = [];

        res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        res.on("end", function () {
            var body = Buffer.concat(chunks);
            labels_tmp = body.toString();
            labels_tmp = JSON.parse(labels_tmp);
            labels_tmp.options.forEach(j => {
                labels.push(j);
            });
            if(arr_labels.length == i+1) {
                console.log('Got Labels')
            }
        });
        });

        req.end();
    }
}

function jsonToken() {
    var data = fs.readFileSync('secretKey.txt');
    jsonSecretkey = data.toString();
}

function createToken(key) {
        var token = jwt.sign({
        data: key.toString()
      }, jsonSecretkey, { expiresIn: '2d' }); // Eg: 1000, "2 days", "10h", "7d"
    return token;
}

function decodeToken(token_) {
    var decoded = jwt.verify(token_, jsonSecretkey);
    return decoded.data;
}

function magentoServer() {
    
    var data = fs.readFileSync('serverkey.txt');
    var splitted = data.toString().split(",");
    var host = splitted.forEach(i => {
        var tp = i.split(":");
        if(tp[1] == ' true') {
            magentoServerKey = 'http://' + tp[0].toString();
            console.log('Current Magento server is ' + magentoServerKey)
        }
    })
}

jsonToken();
magentoServer();
getLabels();


module.exports = router;