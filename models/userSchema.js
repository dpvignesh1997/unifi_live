var mongoose = require('mongoose');
Schema = mongoose.Schema;

var userSchema = mongoose.Schema({
    first_name:  String ,
    last_name: String ,
    posts_ids: [{ type:Schema.Types.ObjectId, ref:'Posts' }]
});

var Users = mongoose.model('Users', userSchema);
module.exports = Users;